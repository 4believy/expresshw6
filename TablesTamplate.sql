INSERT INTO public.groups(name)
  VALUES ('group 1'),('group 2'),('group 3');

INSERT INTO public.courses(name, description, hours)
  VALUES ('course 1','some description for course 1',45),
  ('course 2','some description for course 2',47),
  ('course 3','some description for course 3',80);

INSERT INTO public.lectors( name, email, password)
  VALUES ('lector 1','first_lector1@gmail.com','2123'),
  ('lector 2','second_lector2@gmail.com','21233'),
  ('lector 3','second_lector3@gmail.com','212563');
  
 INSERT INTO public.students(name, surname, email, age, image_path,  group_id)
  VALUES ('student1','Surname 1','someemail1@gmail.com',22, 'path.img', 1),
  ('student2','Surname 2','someemail2@gmail.com',23, 'path2.img', 2),
  ('student3','Surname 3','someemail3@gmail.com',23, 'path3.img', null);

INSERT INTO public.lector_course(lector_id, course_id)
  VALUES (1,2),(1,1),(2,2),(3,2),(3,1);

INSERT INTO public.marks(
   mark, course_id, student_id, lector_id)
  VALUES (61,2,1,1),(61,2,1,1),(99,3,1,2),(81,2,2,1),(77,2,3,3);
  
SELECT * FROM marks