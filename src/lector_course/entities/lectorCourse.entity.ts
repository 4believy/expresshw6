import { Entity, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/course.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse extends CoreEntity {
  @ManyToOne(() => Lector, (lector) => lector.courses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'lector_id', referencedColumnName: 'id' }])
  lectors: Lector[];

  @ManyToOne(() => Course, (course) => course.lectors, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'course_id', referencedColumnName: 'id' }])
  courses: Course[];

  @PrimaryColumn({ name: 'lector_id' })
  lectorId: number;

  @PrimaryColumn({ name: 'course_id' })
  courseId: number;
}
