import { Router } from 'express';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import * as LectorsController from './lectors.controller';
import validator from '../application/middlewares/validation.middleware';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { lectorCreateSchema } from './lector.schema';

const router = Router();

router.get('/', controllerWrapper(LectorsController.getAllLectors));

router.get(
  '/:id/courses',
  validator.params(idParamsSchema),
  controllerWrapper(LectorsController.getLectorCourses),
);

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(LectorsController.getLectorWithCoursesById),
);

router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(LectorsController.createLector),
);

router.patch('/:id/course/:courseId', controllerWrapper(LectorsController.addLectorToCourse));

export default router;
