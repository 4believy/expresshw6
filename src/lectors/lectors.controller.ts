import express from 'express';
import * as LectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';

export const getAllLectors = async (request: express.Request, response: express.Response) => {
  const lectors = await LectorsService.getAllLectors();
  response.json(lectors);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: express.Response,
) => {
  const lector = await LectorsService.createLector(request.body);
  response.json(lector);
};

export const addLectorToCourse = async (
  request: express.Request<{ id: string; courseId: string }>,
  response: express.Response,
) => {
  const { id, courseId } = request.params;
  const lector = await LectorsService.addLectorToCourse(id, courseId);
  response.json(lector);
};

export const getLectorCourses = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const lector = await LectorsService.getLectorCourses(id);
  response.json(lector);
};

export const getLectorWithCoursesById = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const lector = await LectorsService.getLectorWithCoursesById(id);
  response.json(lector);
};
