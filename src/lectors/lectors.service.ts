import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { ILector } from './types/lector.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lector.entity';
import { Course } from '../courses/entities/course.entity';

const lectorsRepository = AppDataSource.getRepository(Lector);
const coursesRepository = AppDataSource.getRepository(Course);

export const getAllLectors = async (): Promise<Lector[]> => {
  return lectorsRepository
    .createQueryBuilder('lector')
    .select([
      'lector.id as id',
      'lector.name as name',
      'lector.email as email',
      'lector.password as password',
    ])
    .getRawMany();
};

export const createLector = async (createLectorSchema: Omit<ILector, 'id'>): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      email: createLectorSchema.email,
    },
  });

  if (lector) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'Lector with this email already exist');
  }

  return lectorsRepository.save(createLectorSchema);
};

// додавання лектора до курсу
export const addLectorToCourse = async (id: string, courseId: string): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({ where: { id: id } });
  const course = await coursesRepository.findOne({
    where: { id: courseId },
    relations: ['lectors'],
  });

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  if (course.lectors?.some((courseLector) => courseLector.id === lector.id)) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'Course already have this lector');
  }

  if (course.lectors && course.lectors.length == 0) {
    course.lectors = [lector];
  } else {
    course.lectors?.push(lector);
  }
  await coursesRepository.save(course);

  return lector;
};

// - отримати всі курси на яких веде лектор по lector_id
export const getLectorCourses = async (id: string): Promise<Lector[]> => {
  const isLectorExist = await lectorsRepository.findOne({
    where: {
      id: id,
    },
  });

  if (!isLectorExist) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'courses')
    .select([
      'courses.id as id',
      'courses.name as name',
      'courses.description as description',
      'courses.hours as hours',
    ])
    .where('lector.id = :id', { id })
    .andWhere('courses.id is NOT NULL')
    .getRawMany();

  if (lector.length === 0) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector has no courses');
  }

  return lector;
};

// - отримати лектора по lector_id із всіма курсами на яких він викладає (має бути вся інформація про лектора із масивом студентів)
// Upd. "Так, мало бути вся інформація про лектора з масивом курсів"
export const getLectorWithCoursesById = async (id: string): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'courses')
    .where('lector.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'lector not found');
  }

  return lector;
};
