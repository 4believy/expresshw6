export interface IStudent {
  id: string;
  email: string;
  name: string;
  surname: string;
  age: number;
  imagePath: string;
  groupId: number;
}

export interface IStudentNameValidation {
  name?: string;
}
