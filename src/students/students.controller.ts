import express from 'express';
import * as StudentsService from './students.service';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentGroupUpdateRequest } from './types/student-group-update-request.interface';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import { IStudentNameValidation } from './types/student.interface';
import { Student } from './entities/student.entity';

export const getAllStudents = async (request: express.Request, response: express.Response) => {
  const { name }: IStudentNameValidation = request.query;
  let students: Student[];
  console.log(name);
  if (name) {
    students = await StudentsService.getAllStudentsWithName(name);
  } else {
    students = await StudentsService.getAllStudents();
  }

  // students = await StudentsService.getAllStudents();
  response.json(students);
};

export const getStudentById = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await StudentsService.getStudentById(id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: express.Response,
) => {
  const student = await StudentsService.createStudent(request.body);
  response.json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await StudentsService.updateStudentById(id, request.body);
  response.json(student);
};

export const updateStudentGroupById = async (
  request: ValidatedRequest<IStudentGroupUpdateRequest>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await StudentsService.updateStudentGroupById(id, request.body);
  response.json(student);
};

export const deleteStudentById = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await StudentsService.deleteStudentById(id);
  response.json(student);
};

// export const addImage = (
//   request: express.Request<{ id: string; file: Express.Multer.File }>,
//   response: express.Response,
// ) => {
//   const { id } = request.params;
//   const { path } = request.file ?? {};
//   const student = StudentsService.addImage(id, path);
//   response.json(student);
// };

// export const getStudentImageById = (
//   request: express.Request<{ id: string }>,
//   response: express.Response,
// ) => {
//   const { id } = request.params;
//   const student = StudentsService.getStudentImageById(id);
//   response.json(student);
// };
