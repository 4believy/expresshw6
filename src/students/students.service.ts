import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

export const studentsRepository = AppDataSource.getRepository(Student);

const studentTableTamplate = [
  'student.id as id',
  'student.name as name',
  'student.surname as surname',
  'student.age as age',
  'student.email as email',
  'student.imagePath as imagePath',
  'student.groupId as groupId',
];

export const getAllStudents = async (): Promise<Student[]> => {
  return await studentsRepository
    .createQueryBuilder('student')
    .select(studentTableTamplate)
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .getRawMany();
};

export const getAllStudentsWithName = async (name: string): Promise<Student[]> => {
  const students = await studentsRepository
    .createQueryBuilder('student')
    .select(studentTableTamplate)
    .where('student.name = :name', { name })
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .getRawMany();

  if (students.length === 0) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'There are no students with this name');
  }
  return students;
};

export const getStudentById = async (id: string): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select(studentTableTamplate)
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: createStudentSchema.email,
    },
  });

  if (student) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'Student with this email already exist');
  }

  return studentsRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  id: string,
  UpdateStudentSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, UpdateStudentSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

// додати групу студенту
export const updateStudentGroupById = async (
  id: string,
  UpdateStudentGroupSchema: Pick<IStudent, 'groupId'>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, UpdateStudentGroupSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return result;
};
