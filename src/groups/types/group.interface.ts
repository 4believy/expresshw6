import { IStudent } from '../../students/types/student.interface';

export interface IGroup {
  id: string;
  name: string;
}
