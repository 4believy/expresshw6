import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation';
import { IGroup } from './group.interface';
export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<IGroup, 'name'>;
}
