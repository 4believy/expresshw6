import { Router } from 'express';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import * as GroupsController from './groups.controller';
import validator from '../application/middlewares/validation.middleware';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { groupCreateSchema } from './group.schema';
import { groupUpdateSchema } from './group.schema';

const router = Router();

router.get('/', controllerWrapper(GroupsController.getAllGroups));

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(GroupsController.getGroupById),
);

router.get(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(GroupsController.getGroupById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(GroupsController.createGroup),
);

router.patch(
  '/:id',
  validator.params(idParamsSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(GroupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idParamsSchema),
  controllerWrapper(GroupsController.deleteGroupById),
);

export default router;
