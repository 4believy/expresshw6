import express from 'express';
import * as GroupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';

export const getAllGroups = async (request: express.Request, response: express.Response) => {
  const groups = await GroupsService.getAllGroups();
  response.json(groups);
};

export const getGroupById = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const group = await GroupsService.getGroupById(id);
  response.json(group);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: express.Response,
) => {
  const group = await GroupsService.createGroup(request.body);
  response.json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: express.Response,
) => {
  const { id } = request.params;
  const group = await GroupsService.updateGroupById(id, request.body);
  response.json(group);
};

export const deleteGroupById = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const group = await GroupsService.deleteGroupById(id);
  response.json(group);
};
