import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IGroup } from './types/group.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Group } from './entities/group.entity';
import { DeleteResult, UpdateResult } from 'typeorm';

const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  return await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .select(['group.id', 'group.name', 'student'])
    .getMany();
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .select(['group.id', 'group.name', 'student'])
    .where('group.id = :id', { id })
    .getOne();

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'group not found');
  }

  return group;
};

export const createGroup = async (createGroupSchema: Omit<IGroup, 'id'>): Promise<Group> => {
  return groupsRepository.save(createGroupSchema);
};

export const updateGroupById = async (
  id: string,
  UpdateGroupSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(id, UpdateGroupSchema);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const result = await groupsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  return result;
};
