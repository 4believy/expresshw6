import express from 'express';
import * as CourseService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ICourseCreateRequest } from './types/course-create-request.interface';

export const getAllCourses = async (request: express.Request, response: express.Response) => {
  const courses = await CourseService.getAllCourses();
  response.json(courses);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: express.Response,
) => {
  const course = await CourseService.createCourse(request.body);
  response.json(course);
};
