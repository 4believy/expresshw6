import { ICourse } from './types/course.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/course.entity';

export const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  return coursesRepository
    .createQueryBuilder('course')
    .select([
      'course.id as id',
      'course.name as name',
      'course.description as description',
      'course.hours as hours ',
    ])
    .getRawMany();
};

export const createCourse = async (createCourseSchema: Omit<ICourse, 'id'>): Promise<Course> => {
  return coursesRepository.save(createCourseSchema);
};
