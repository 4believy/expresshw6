import { Router } from 'express';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import * as CoursesController from './courses.controller';
import validator from '../application/middlewares/validation.middleware';
import { courseCreateSchema } from './course.schema';

const router = Router();

router.get('/', controllerWrapper(CoursesController.getAllCourses));

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(CoursesController.createCourse),
);

export default router;
