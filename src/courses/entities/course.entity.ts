import { Entity, Column, OneToMany, ManyToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Mark } from '../../marks/entities/marks.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  lectors?: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
