import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import StudentsRouter from '../students/students.router';
import GroupsRouter from '../groups/groups.router';
import CoursesRouter from '../courses/courses.router';
import LectorsRouter from '../lectors/lectors.router';
import MarksRouter from '../marks/marks.router';
import bodyParser from 'body-parser';
import exceptionsFilter from './middlewares/exceptions.filter';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);
AppDataSource.initialize()
  .then(() => console.log('Typeorm connected to database'))
  .catch((error) => console.log('Error: ', error));

const StaticFilePath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(StaticFilePath));
app.use('/api/v1/students', StudentsRouter);
app.use('/api/v1/groups', GroupsRouter);
app.use('/api/v1/courses', CoursesRouter);
app.use('/api/v1/lectors', LectorsRouter);
app.use('/api/v1/marks', MarksRouter);

app.use(exceptionsFilter);

export default app;
