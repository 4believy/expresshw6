import { Router } from 'express';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import * as MarksController from './marks.controller';
import validator from '../application/middlewares/validation.middleware';
import { idParamsSchema } from '../application/schemas/id-param.schema';
import { markCreateSchema } from './mark.schema';

const router = Router();

router.post('/', validator.body(markCreateSchema), controllerWrapper(MarksController.createMark));

router.get(
  '/student/:id',
  validator.params(idParamsSchema),
  controllerWrapper(MarksController.getMarksByStudentId),
);

router.get(
  '/course/:id',
  validator.params(idParamsSchema),
  controllerWrapper(MarksController.getMarksByCourseId),
);

export default router;
