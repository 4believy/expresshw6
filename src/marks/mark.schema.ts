import Joi from 'joi';
import { IMark } from './types/mark.interface';

export const markCreateSchema = Joi.object<Omit<IMark, 'id'>>({
  courseId: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
  mark: Joi.number().min(1).max(101).required(),
});
