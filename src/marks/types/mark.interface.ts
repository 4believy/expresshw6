export interface IMark {
  id: number;
  courseId: number;
  studentId: number;
  lectorId: number;
  mark: number;
}
