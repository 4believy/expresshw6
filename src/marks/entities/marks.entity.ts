import { Entity, Column, OneToMany, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { Course } from '../../courses/entities/course.entity';
import { Lector } from '../../lectors/entities/lector.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'numeric',
    nullable: false,
  })
  mark: number;

  //course
  @Column({ name: 'course_id' })
  courseId: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  //student
  @Column({ name: 'student_id' })
  studentId: number;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: true,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  //lector
  @Column({ name: 'lector_id' })
  lectorId: number;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;
}
