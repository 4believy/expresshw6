import { HttpStatuses } from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IMark } from './types/mark.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Mark } from './entities/marks.entity';
import { studentsRepository } from '../students/students.service';
import { coursesRepository } from '../courses/courses.service';

const markRepository = AppDataSource.getRepository(Mark);

export const createMark = async (createMarkSchema: Omit<IMark, 'id'>): Promise<Mark> => {
  return markRepository.save(createMarkSchema);
};

// - отримати всі оцінки студента по student_id (має містити назву курсу і оцінку)
export const getMarksByStudentId = async (id: string): Promise<Mark[]> => {
  const isStudentExist = await studentsRepository.findOne({
    where: {
      id: id,
    },
  });

  if (!isStudentExist) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const marks = await markRepository
    .createQueryBuilder('mark')
    .leftJoinAndSelect('mark.course', 'course')
    .leftJoinAndSelect('mark.student', 'student')
    .where('student.id = :id', { id })
    .select(['course.name as course_Name', 'mark.mark as mark'])
    .getRawMany();

  if (marks.length === 0) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student has no marks');
  }

  return marks;
};

// - отримати всі оцінки по курсу використовуючи course_id (має містити назву курсу,
//   ім’я лектора, хто поставив оцінку, ім’я студента та саму оцінку)

export const getMarksByCourseId = async (id: string): Promise<Mark[]> => {
  const isCourseExist = await coursesRepository.findOne({
    where: {
      id: id,
    },
  });

  if (!isCourseExist) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }

  const marks = await markRepository
    .createQueryBuilder('mark')
    .leftJoinAndSelect('mark.lector', 'lector')
    .leftJoinAndSelect('mark.student', 'student')
    .leftJoinAndSelect('mark.course', 'course')
    .where('course.id = :id', { id })
    .select(['lector.name as lector_Name', 'student.name as student_Name', 'mark.mark as mark'])
    .getRawMany();

  if (marks.length === 0) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course has no marks');
  }

  return marks;
};
