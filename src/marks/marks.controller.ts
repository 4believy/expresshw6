import express from 'express';
import * as MarksService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IMarkCreateRequest } from './types/mark-create-request.interface';

export const createMark = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: express.Response,
) => {
  const course = await MarksService.createMark(request.body);
  response.json(course);
};

export const getMarksByStudentId = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await MarksService.getMarksByStudentId(id);
  response.json(student);
};

export const getMarksByCourseId = async (
  request: express.Request<{ id: string }>,
  response: express.Response,
) => {
  const { id } = request.params;
  const student = await MarksService.getMarksByCourseId(id);
  response.json(student);
};
