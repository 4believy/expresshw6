import 'dotenv/config';
import app from './application/app';

const port = process.env.SERVER_PORT || 3000;

const startServer = async () => {
  app.listen(port, () => {
    console.log(`server started at port ${port}`);
  });
};

startServer();
